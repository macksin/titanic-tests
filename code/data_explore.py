import pandas as pd
import os


def transform(df):
    # Basic Feature Engineering
    df['cabin_encoding'] = df.Cabin.str[0]
    df.loc[df.cabin_encoding.isnull(), 'cabin_encoding'] = 'M'
    # names
    df['isMr'] = df.Name.str.contains('Mr.')
    df['isMrs'] = df.Name.str.contains('Mrs.')
    df['isMiss'] = df.Name.str.contains('Miss.')
    return df


for path, _, filename in os.walk('../src'):
    for file in filename:
        filepath = os.path.join(path, file)
        print(file)

        if file not in ['train.csv', 'test.csv']:
            continue

        df = pd.read_csv(filepath)
        df = transform(df)

        filepath2 = os.path.join(path, 't_' + file)
        df.to_csv(filepath2, index=False, encoding='utf-8')
