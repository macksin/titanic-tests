from lightgbm import LGBMClassifier
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectKBest, chi2, SelectFromModel
from sklearn.pipeline import Pipeline
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import train_test_split
from sklearn.impute import SimpleImputer
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

df_train = pd.read_csv('../src/t_train.csv',
                       encoding='utf-8')
df_test = pd.read_csv('../src/t_test.csv',
                      encoding='utf-8')
df_test.head()


numerical = ['Age', 'SibSp', 'Parch', 'Fare', 'isMr', 'isMrs', 'isMiss']
categorical = ['Pclass', 'Sex', 'Embarked', 'cabin_encoding']
target = ['Survived']

x, y = df_train[numerical+categorical], df_train[target]


x = pd.get_dummies(x, columns=categorical, drop_first=True)

imr = SimpleImputer(missing_values=np.nan, strategy='most_frequent')
imr = imr.fit(x)
x = imr.transform(x)


x_train, x_test, y_train, y_test = train_test_split(
    x, y, test_size=0.20, random_state=42, stratify=y)


# Select K Best
params = {'sfm__max_features': range(2, x.shape[1] + 1),
          'clf__n_estimators': range(1, 500),
          'clf__max_depth': range(2, 10)}
classifier_1 = Pipeline([('sfm', SelectFromModel(LGBMClassifier())),
                         ('clf', LGBMClassifier())])

grid = RandomizedSearchCV(classifier_1,
                          params,
                          n_iter=50,
                          scoring='roc_auc',
                          n_jobs=5,
                          cv=4,
                          return_train_score=True)

grid.fit(x_train, y_train.values.ravel())

results = pd.DataFrame(grid.cv_results_)
results.sort_values(by=['param_sfm__max_features',
                    'rank_test_score'], ascending=True, inplace=True)
results.drop_duplicates(
    subset=['param_sfm__max_features'], keep='first', inplace=True)

results_plot = results.loc[:, [
    'param_sfm__max_features', 'mean_test_score', 'mean_train_score']]
results_plot.set_index('param_sfm__max_features', inplace=True)
# Select best within margin
threshold = results_plot.mean_test_score.max() - 0.01
best_est = results_plot.loc[results_plot.mean_test_score >=
                            threshold, :].index.min()

print("Best Parameters")
print("\t", results.loc[results.param_sfm__max_features ==
      best_est].params.values[0])


results_plot.plot()
plt.grid()
plt.axvline(best_est, color='red', ls='--')
plt.show()


est = grid.best_estimator_
print("Melhor modelo")
print(grid.best_params_)
prob = est.predict_proba(x_test)[:, 1]
print("TEST  %4.2f" % (roc_auc_score(y_test, prob) * 100))
prob = est.predict_proba(x_train)[:, 1]
print("TRAIN %4.2f" % (roc_auc_score(y_train, prob) * 100))

classifier_1

param_simples = results.loc[results.param_sfm__max_features == best_est].params.values[0]

classifier.fit(x_train, y_train)
print("Melhor modelo simples")
print(param_simples)
prob = classifier.predict_proba(x_test)[:, 1]
print("TEST  %4.2f" % (roc_auc_score(y_test, prob) * 100))
prob = classifier.predict_proba(x_train)[:, 1]
print("TRAIN %4.2f" % (roc_auc_score(y_train, prob) * 100))
