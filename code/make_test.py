import numpy as np
from matplotlib import pyplot as plt

# Semente - Repetitibilidade!!!
np.random.seed(0)

# Numeros de exemplo
x = np.random.randint(1, 10, 100)
y = np.random.randint(1, 10, 100)

# Plotagem
plt.title("Pontos randomicos")
plt.scatter(x, y)
plt.tight_layout()
plt.show()
